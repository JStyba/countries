import { Validators } from "@angular/forms";

// possible way to use a custom validator in a seperate file
const syncValidator = (control: any): {[key: string]: boolean} | null =>{
    if (control?.value?.toLowerCase() === 'russia') {
        return {countryNameForbidden: true}
    } else {
        return null;
    }
}

export const COUNTRY_FORM = {
    id: null,
    countryName: [null, Validators.required, syncValidator],
    population: null,
    flag: null,
    countryCapital: null
  }