export interface ICountry {
    id: number;
    countryName: string;
    population: number;
    flag: string;
    countryCapital: string;
}