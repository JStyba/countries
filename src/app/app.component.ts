import { Component } from '@angular/core';
import { text } from 'src/assets/data';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'front-back';
 textFromParent: string = text;

}
