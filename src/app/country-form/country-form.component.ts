import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from '../http.service';
import { ICountry } from './country.model';

@Component({
  selector: 'app-country-form',
  templateUrl: './country-form.component.html',
  styleUrls: ['./country-form.component.scss']
})
export class CountryFormComponent implements OnInit {
  COUNTRY_FORM = {
    id: null,
    countryName: [null, [Validators.required, this.forbiddenCountryName]],
    population: null,
    flag: null,
    countryCapital: [null, [], this.forbiddenCapitalName]
  }
  form: FormGroup;
  isEditing: boolean = false;
  arrayOfFlags = [{name: 'Poland', value: "../../assets/poland.png"}, {name: 'Germany', value: '../../assets/germany.png'}, {name: 'Bhutan', value: '../../assets/bhutanFlag.png'}, {name: 'Nepal', value: '../../assets/nepalFlag.png'}, {name: 'China', value: '../../assets/chinaFlag.png'}, {name: 'India', value: '../../assets/indiaFlag.png'}]
  constructor(private httpService: HttpService, private route: ActivatedRoute, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.createForm();
    this.route.params.subscribe(params => {
      if (params.id) {
        this.isEditing = true;
        this.httpService.getCountry(params.id).subscribe((c: ICountry) => {
          // IF YOU WANT TO USE A DIFFERENT MODEL, CHANGE THE MODEL HERE
          // this.form.setValue({
          //   id: c.id,
          //   countryName: c.countryName,
          //   population: c.population,
          //   flag: c.flag,
          //   countryCapital: c.countryCapital
          // });
          this.form.setValue(c);
        });
      }
    })
  }

  forbiddenCountryName(control: FormControl): {[key: string]: boolean} | null{
    if (control?.value?.toLowerCase() === 'russia') {
      return {countryNameForbidden: true}
    } else {
      return null;
    }
  }



  forbiddenCapitalName(
    control: FormControl
  ): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
    const promise = new Promise<ValidationErrors | null>((resolve, reject) => {
      setTimeout(() => {
        if (control.value?.toLowerCase() === 'paris') {
          resolve({ forbiddenCapital: true });
        } else {
          resolve(null);
        }
      }, 2000);
    });
    return promise;
  }

  onSubmit(): void {
    if (this.form.valid) {
      if (this.isEditing) {
        this.httpService.editCountry(this.form.value).subscribe(() => {
          this.form.reset();
        });
      } else {
        this.httpService.addCountry(this.form.value).subscribe(() => {
          this.form.reset();
        })
      }
    } else {
      alert('Don\'t hack my website!')
    }
  }
  
  createForm(): void {
    // this.form = new FormGroup({
    //   id: new FormControl(),
    //   countryName: new FormControl(),
    //   population: new FormControl(),
    //   flag: new FormControl(),
    //   countryCapital: new FormControl()
    // })
    this.form = this.fb.group({...this.COUNTRY_FORM});
  }
  

}
