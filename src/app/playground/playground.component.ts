import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.scss']
})
export class PlaygroundComponent implements OnInit {
  @Input() textFromChild: string;
  styleVariable1 = false;
  styleVariable2 = false;
  styleVariable = {
    'styleI': this.styleVariable1,
    'styleII': this.styleVariable2
}
  constructor() { }

  ngOnInit(): void {
  }

}
