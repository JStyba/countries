import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {
  countries: any[] = [];
  constructor(private httpService: HttpService) {}

  ngOnInit(): void {
    this.getCountries();
  }

  deleteCountry(id: number): void {
    this.httpService.deleteCountry(id).subscribe(() => {
      this.getCountries();
    });
  }

  getCountries(): void {
    this.httpService.getCountries().subscribe((c: any[]) => {
      this.countries = c;
    });
  }
}
