import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ICountry } from './country-form/country.model';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
baseUrl: string = 'http://localhost:8080'
  constructor(private http: HttpClient) { }

  getCountries(): Observable<unknown> {
    return this.http.get(`${this.baseUrl}/countries`);
  }

  getCountry(id: number): Observable<unknown> {
    return this.http.get(`${this.baseUrl}/country/${id}`);
  }

  deleteCountry(id: number): Observable<unknown> {
    return this.http.delete(`${this.baseUrl}/country/${id}`)
  }

  addCountry(country: ICountry): Observable<unknown> {
    return this.http.post(`${this.baseUrl}/countries`, country);
  }

  editCountry(country: ICountry): Observable<unknown> {
    return this.http.put(`${this.baseUrl}/countries/`, country);
  }

}
