import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CountryFormComponent } from './country-form/country-form.component';
import { PlaygroundComponent } from './playground/playground.component';
import { TableComponent } from './table/table.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'table', component: TableComponent },
  { path: 'playground', component: PlaygroundComponent },
  { path: 'country-form/add', component: CountryFormComponent },
  { path: 'country-form/edit/:id', component: CountryFormComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
